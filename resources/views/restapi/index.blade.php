@extends('layouts.app')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">Rest API</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                  <p>Use the Postman application to test this URL, if successful it will display data in the user table.</p>
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Method</th>
                            <th>Url</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>GET</td>
                            <td>http://localhost:8000/api/v1/posts/[ID]</td>
                            <td>Data search based on ID<br>
                              [ID] is the ID data that will be searched in the table</td>
                        </tr>
                        <tr>
                          <td>POST</td>
                          <td>http://localhost:8000/api/v1/posts/store</td>
                          <td>Used to input data, in Postman add Body>form-data with 'key title' and 'content', in value enter the data to be input</td>
                        </tr>
                        <tr>
                          <td>GET</td>
                          <td>http://127.0.0.1:8000/api/v1/posts</td>
                          <td>Used to display all data in the table</td>
                        </tr>
                        <tr>
                          <td>POST</td>
                          <td>http://localhost:8000/api/v1/posts/update</td>
                          <td>Used to update data, in Postman add Body>form-data with 'key title' and 'content', in the value enter the data to be input</td>
                        </tr>
                        <tr>
                          <td>DELETE</td>
                          <td>http://localhost:8000/api/v1/posts/[ID]</td>
                          <td>Delete data by ID.<br>
                            [ID] is the ID data that will be deleted in the table</td>
                        </tr>
                    </tbody>
                  </table>
                </div>
                <!-- /.card-body -->
              </div>
              <!-- /.card -->
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection