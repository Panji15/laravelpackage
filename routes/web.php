<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    // return view('admin/adminlte');
    return view('auth/login');

});

Route::get('/dashboard', function () {
    return view('admin/adminlte');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('kontak', 'KontakController');
// Route::get('kontak', 'KontakController@index'); 
// Route::get('/kontak/json', 'KontakController@datajson')->name('json');
Route::resource('sample', 'SampleController');

Route::post('sample/update', 'SampleController@update')->name('sample.update');

Route::get('sample/destroy/{id}', 'SampleController@destroy');

//Masteruser

Route::resource('masteruser', 'MasteruserController');
Route::resource('restapi', 'RestapiController');
