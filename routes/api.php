<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/v1/posts/store', 'api\v1\PostsController@store');
Route::get('/v1/posts', 'api\v1\PostsController@index');
Route::get('/v1/posts/{id?}', 'api\v1\PostsController@show');
Route::post('/v1/posts/update', 'api\v1\PostsController@update');
Route::delete('/v1/posts/{id?}', 'api\v1\PostsController@destroy');

// Route::post('/v1/apiuser/store', 'api\v1\ApiuserController@store');
Route::get('/v1/apiuser', 'api\v1\ApiuserController@index');
// Route::get('/v1/apiuser/{id?}', 'api\v1\ApiuserController@show');
// Route::post('/v1/apiuser/update', 'api\v1\ApiuserController@update');
// Route::delete('/v1/apiuser/{id?}', 'api\v1\ApiuserController@destroy');

// Route::get('/apiuser', [App\Http\Controllers\API\ApiuserController::class, 'getAllUser']);
// Route::get('/apiuser', 'api\ApiuserController@index');

