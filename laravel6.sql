-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             12.5.0.6677
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for laravel6
CREATE DATABASE IF NOT EXISTS `laravel6` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `laravel6`;

-- Dumping structure for table laravel6.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel6.failed_jobs: ~0 rows (approximately)

-- Dumping structure for table laravel6.kontaks
CREATE TABLE IF NOT EXISTS `kontaks` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `nama_lengkap` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pekerjaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel6.kontaks: ~2 rows (approximately)
INSERT INTO `kontaks` (`id`, `nama_lengkap`, `email`, `pekerjaan`, `alamat`, `created_at`, `updated_at`) VALUES
	(1, 'name is', 'name@gmail.com', 'IT', 'Lippo Kuningan B15 LT16', NULL, NULL),
	(4, 'Testing', 'test@gmail.com', 'Staf', 'Lippo Kuningan', NULL, NULL);

-- Dumping structure for table laravel6.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel6.migrations: ~4 rows (approximately)
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2019_08_19_000000_create_failed_jobs_table', 1),
	(4, '2023_10_16_162708_create_kontaks_table', 2),
	(5, '2023_10_17_061009_create_posts_table', 3);

-- Dumping structure for table laravel6.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel6.password_resets: ~0 rows (approximately)

-- Dumping structure for table laravel6.posts
CREATE TABLE IF NOT EXISTS `posts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel6.posts: ~3 rows (approximately)
INSERT INTO `posts` (`id`, `title`, `content`, `created_at`, `updated_at`) VALUES
	(2, 'test1', 'post', '2023-10-17 01:18:00', '2023-10-17 01:18:00'),
	(3, 'test2', 'post2', '2023-10-17 01:28:09', '2023-10-17 01:28:09'),
	(4, 'test3', 'post3', '2023-10-17 01:28:23', '2023-10-17 01:28:23');

-- Dumping structure for table laravel6.sample_datas
CREATE TABLE IF NOT EXISTS `sample_datas` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel6.sample_datas: ~18 rows (approximately)
INSERT INTO `sample_datas` (`id`, `first_name`, `last_name`, `created_at`, `updated_at`) VALUES
	(2, 'Peter', 'Parker', '2019-10-11 14:39:09', '2019-10-11 14:39:09'),
	(3, 'Larry', 'Degraw', '2019-10-11 14:39:09', '2019-10-11 14:39:09'),
	(4, 'Tabitha', 'Russell', '2019-10-11 14:39:09', '2019-10-11 14:39:09'),
	(5, 'Susan', 'Diener', '2019-10-13 17:30:00', '2019-10-13 17:30:00'),
	(6, 'William', 'Batiste', '2019-10-13 17:30:00', '2019-10-13 17:30:00'),
	(7, 'Bessie', 'Tucker', '2019-10-13 17:30:00', '2019-10-13 17:30:00'),
	(8, 'Eva', 'King', '2019-10-13 17:30:00', '2019-10-13 17:30:00'),
	(9, 'Dorothy', 'Hays', '2019-10-13 20:30:00', '2019-10-13 20:30:00'),
	(10, 'Nannie', 'Ayers', '2019-10-13 20:30:00', '2019-10-13 20:30:00'),
	(11, 'Gerald', 'Brown', '2019-10-13 21:30:00', '2019-10-13 21:30:00'),
	(12, 'Judith', 'Smithlaw', '2019-10-13 21:30:00', '2023-10-18 01:32:25'),
	(13, 'Betty', 'McLaughlin', '2019-10-14 06:30:00', '2019-10-14 06:30:00'),
	(14, 'Delores', 'Schumacher', '2019-10-14 06:30:00', '2019-10-14 06:30:00'),
	(15, 'Gloria', 'Romero', '2019-10-13 23:30:00', '2019-10-13 23:30:00'),
	(16, 'Bobbie', 'Wilson', '2019-10-13 23:30:00', '2019-10-13 23:30:00'),
	(17, 'Paul', 'Pate', '2019-10-14 06:30:00', '2019-10-14 06:30:00'),
	(18, 'Ryan', 'Hoang', '2019-10-14 06:30:00', '2019-10-14 06:30:00'),
	(19, 'Doni', 'Saputre', '2023-10-18 01:52:17', '2023-10-18 01:52:17');

-- Dumping structure for table laravel6.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table laravel6.users: ~3 rows (approximately)
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'admin@gmail.com', NULL, '$2y$10$z8MQvDX.wLMZBJFdLftxkuqEHOotuvfOTZcHeRupzMbenioYRPoRS', '3nQgx6zcsFgsQO4qLWgXTaeyEjinFYqM2eypTnJUy2OlUNzpZbF6QAByes87', '2023-10-16 09:23:12', '2023-10-16 09:23:12'),
	(3, 'Testing19102023', 'test@gmail.com', NULL, '$2y$10$v534WRRguoIoi647EYgy.OVYXpOQI4dVriixaqSUplN6jYtJQFy5q', NULL, NULL, NULL),
	(4, 'IVOJI', 'testingivoji@gmail.com', NULL, '$2y$10$36EvyniA9pJhG3EoTjZ4leopsg.Mzf6wGqanTbEe1VwjMc5F.h9zW', NULL, NULL, NULL);

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
