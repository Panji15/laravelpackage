<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apiuser extends Model
{
    protected $fillable = [
            'name', 'email'
    ];
}
