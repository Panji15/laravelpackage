<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Datatables; //pemanggilan datatable dalam sebuah controller
// use App\Kontak;

class KontakController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function index()
    {
        $data = array();
        $data['kontak'] = DB::table('kontaks')->get();
        return view('kontak.index',$data);
        // return view('kontak.index');
    }

    // public function datajson() 
    // { 
    //     return Datatables::of(Kontak::all())->make(true); 
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kontak.create');    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //pertama cek validasi
        $this->validate($request,[
            'nama_lengkap'  => 'required',
            'email'         => 'required',
            'pekerjaan'     => 'required',
            'alamat'        => 'required'
        ]);

        //selanjutnya insert ke database
        DB::table('kontaks')->insert([
            'nama_lengkap'  => $request->nama_lengkap,
            'email'         => $request->email,
            'pekerjaan'     => $request->pekerjaan,
            'alamat'        => $request->alamat
        ]);

        //setelah berhasil insert di redirect
        return redirect('/kontak')->with('status','Data Kontak Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //mengambil data dari database
        $kontak = DB::table('kontaks')->where('id',$id)->first();
        
        //passing data ke view edit.blade.php
        return view('kontak.edit',compact('kontak'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //mengupdate data
        
        DB::table('kontaks')->where('id',$id)->update([
            'nama_lengkap'  => $request->nama_lengkap,
            'email'         => $request->email,
            'pekerjaan'     => $request->pekerjaan,
            'alamat'        => $request->alamat
        ]);
                
        //redirect setelah berhasil menjalankan update
        return redirect('/kontak')->with('status','Data Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //menghapus data
        DB::table('kontaks')->where('id',$id)->delete();
        //redirect setelah menghapus
        return redirect('/kontak')->with('status','Data Berhasil Dihapus');
        // //delete post by ID
        // Post::where('id', $id)->delete();

        // //return response
        // return response()->json([
        //     'success' => true,
        //     'message' => 'Data Post Berhasil Dihapus!.',
        // ]); 
    }
}
