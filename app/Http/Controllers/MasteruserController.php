<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Datatables; //pemanggilan datatable dalam sebuah controller
use Illuminate\Support\Facades\Hash;

class MasteruserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    public function index()
    {
        $data = array();
        $data['masteruser'] = DB::table('users')->get();
        return view('masteruser.index',$data);
        // return view('kontak.index');
    }

    // public function datajson() 
    // { 
    //     return Datatables::of(Kontak::all())->make(true); 
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('masteruser.create');    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //pertama cek validasi
        $this->validate($request,[
            'name'          => 'required',
            'email'         => 'required',
            'password'      => 'required',
        ]);

        //selanjutnya insert ke database
        DB::table('users')->insert([
            'name'          => $request->name,
            'email'         => $request->email,
            'password'      => Hash::make($request->password)
        ]);

        //setelah berhasil insert di redirect
        return redirect('/masteruser')->with('status','Data Kontak Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //mengambil data dari database
        $masteruser = DB::table('users')->where('id',$id)->first();
        
        //passing data ke view edit.blade.php
        return view('masteruser.edit',compact('masteruser'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //mengupdate data
        
        DB::table('users')->where('id',$id)->update([
            'name'          => $request->name,
            'email'         => $request->email,
            'password'      => Hash::make($request->password)        
        ]);
                
        //redirect setelah berhasil menjalankan update
        return redirect('/masteruser')->with('status','Data Berhasil Diupdate');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //menghapus data
        DB::table('users')->where('id',$id)->delete();
        //redirect setelah menghapus
        return redirect('/masteruser')->with('status','Data Berhasil Dihapus');
        // //delete post by ID
        // Post::where('id', $id)->delete();

        // //return response
        // return response()->json([
        //     'success' => true,
        //     'message' => 'Data Post Berhasil Dihapus!.',
        // ]); 
    }
}
